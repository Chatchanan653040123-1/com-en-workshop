/**
 * main
 */
public class main {

    public static void main(String[] args) {
        System.out.println("Welcome to the main function\nWhat do you want to do?\n1. Calculator\n2. Hello");
        int choice = Integer.parseInt(System.console().readLine());
        if (choice == 1) {
            Calculator calc = new Calculator();
            calc.calculatorFunction();
        } else if (choice == 2) {
            Hello hello = new Hello();
            System.out.println("Enter your name");
            String name = System.console().readLine();
            System.out.println("Enter your last name");
            String lastName = System.console().readLine();
            hello.helloFunction(name, lastName);
        } else {
            System.out.println("Invalid choice");
        }
    }
}