public class Calculator {
    public void calculatorFunction() {
        System.out.println("Welcome to Calculator function");
        while (true) {
            System.out.println("Enter the first number");
            int a = Integer.parseInt(System.console().readLine());
            System.out.println("Enter the second number");
            int b = Integer.parseInt(System.console().readLine());
            System.out.println("Enter the operation to be performed");
            String op = System.console().readLine();
            if (op.equals("+")) {
                System.out.println("The sum is " + (a + b));
            } else if (op.equals("-")) {
                System.out.println("The difference is " + (a - b));
            } else if (op.equals("*")) {
                System.out.println("The product is " + (a * b));
            } else if (op.equals("/")) {
                System.out.println("The quotient is " + (a / b));
            } else if (op.equals("%")) {
                System.out.println("The remainder is " + (a % b));
            } else {
                System.out.println("Invalid operation");
            }
            System.out.println("Do you want to continue? (y/n)");
            String cont = System.console().readLine();
            if (cont.equals("n")) {
                break;
            } else if (cont.equals("y")) {
                continue;
            } else {
                System.out.println("Invalid choice");
                break;
            }
        }
    }
}
